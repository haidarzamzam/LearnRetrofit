package com.example.haidar.learnretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class ImageActivity extends AppCompatActivity {

    public static final String EXTRA_NAMA = "extraname";
    public static final String EXTRA_IMAGE = "extraimage";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar()!=null){
            getSupportActionBar().setElevation(5);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getIntent().getExtras().getString(EXTRA_NAMA));
        }

        ImageView imgView = findViewById(R.id.imageView);

        Log.d("LOG", EXTRA_IMAGE);
        Picasso.with(this).load(getIntent().getExtras().getString(EXTRA_IMAGE)).into(imgView);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                ImageActivity.this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
