package com.example.haidar.learnretrofit.viewmodels;

import android.app.ProgressDialog;
import android.content.Context;
import android.databinding.ObservableField;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.haidar.learnretrofit.Interface.DataInterface;
import com.example.haidar.learnretrofit.R;
import com.example.haidar.learnretrofit.models.DataTempatModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ViewModelDataTempat {
    public ObservableField<String> nama = new ObservableField<>();
    public ObservableField<String> gambar = new ObservableField<>();

    private DataTempatModel mTempat;
    private Context context;
    private ListView lvTempat;

    public ProgressDialog pDialog;
    public String[] dataGambar;
    public ArrayAdapter<String> adapter;

    public ViewModelDataTempat(Context ctx, ListView lvTempat, ArrayAdapter<String> adapter){
        this.context = ctx;
        this.lvTempat = lvTempat;
        this.adapter = adapter;
        mTempat = new DataTempatModel("Mekkah","https://media.kompas.tv/library/image/thumbnail/116579/1527904200800.a_675_380.png");
        this.nama.set(mTempat.getNama());
        this.gambar.set(mTempat.getGambar());

    }

    public void loadTempat() {
        
        pDialog.setMessage("Please Wait...");
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();

        Retrofit retrofit = new Retrofit.Builder().baseUrl(context.getString(R.string.url_json))
                .addConverterFactory(GsonConverterFactory.create()).build();

        DataInterface dataInterface = retrofit.create(DataInterface.class);

        Call<List<DataTempatModel>> calData = dataInterface.callTempat();
        calData.enqueue(new Callback<List<DataTempatModel>>() {
            @Override
            public void onResponse(Call<List<DataTempatModel>> call, Response<List<DataTempatModel>> response) {
                if (response.isSuccessful()) {
                    int i = 0;
                    dataGambar = new String[response.body().size()];
                    for (DataTempatModel temmpat:response.body()){
                        adapter.add(temmpat.getNama());
                        dataGambar[i] = temmpat.getGambar();
                        i++;
                    }
                    lvTempat.setAdapter(adapter);
                    pDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<List<DataTempatModel>> call, Throwable t) {

            }
        });
    }

}
