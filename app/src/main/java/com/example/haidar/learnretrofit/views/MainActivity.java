package com.example.haidar.learnretrofit.views;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;

import com.example.haidar.learnretrofit.ImageActivity;
import com.example.haidar.learnretrofit.Interface.DataInterface;
import com.example.haidar.learnretrofit.R;
import com.example.haidar.learnretrofit.databinding.ActivityMainBinding;
import com.example.haidar.learnretrofit.models.DataTempatModel;
import com.example.haidar.learnretrofit.viewmodels.ViewModelDataTempat;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding tmpBinding;
    private ViewModelDataTempat vmTempat;

    private ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tmpBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        vmTempat = new ViewModelDataTempat(this, tmpBinding.listviewTempat, adapter);
        tmpBinding.setTempat(vmTempat);

        setSupportActionBar(tmpBinding.toolbar);
        if (getSupportActionBar()!= null){
            getSupportActionBar().setElevation(5);
        }

        vmTempat.adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
        vmTempat.pDialog= new ProgressDialog(this);

        vmTempat.loadTempat();

        tmpBinding.listviewTempat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("Gambar",vmTempat.dataGambar[position]);
                Log.e("Nama", vmTempat.adapter.getItem(position));
                Intent i = new Intent(MainActivity.this, ImageActivity.class);
                i.putExtra(ImageActivity.EXTRA_NAMA, vmTempat.adapter.getItem(position));
                i.putExtra(ImageActivity.EXTRA_IMAGE, vmTempat.dataGambar[position]);
                startActivity(i);
            }
        });
    }
}
