package com.example.haidar.learnretrofit.models;


import com.google.gson.annotations.SerializedName;

public class DataTempatModel {

    @SerializedName("caption")
    private String nama;
    @SerializedName("image")
    private String gambar;

    public DataTempatModel(String nama, String gambar) {
        this.nama = nama;
        this.gambar = gambar;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
