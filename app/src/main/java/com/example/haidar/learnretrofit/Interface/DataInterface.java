package com.example.haidar.learnretrofit.Interface;

import com.example.haidar.learnretrofit.models.DataTempatModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DataInterface {

    @GET("tempat.json")
    Call<List<DataTempatModel>> callTempat();

}
